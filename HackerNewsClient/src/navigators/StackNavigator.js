import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'

import TopStoriesScreen from '../components/screens/TopStoriesScreen'
import CommentScreen from '../components/screens/CommentScreen'

const screens = {
  Home: {
    screen: TopStoriesScreen
  },
  Comments: {
    screen: CommentScreen
  }
}

const homeStack = createStackNavigator(screens);

export default createAppContainer(homeStack);