export const timeAgo = (timeStamp) => {
  var dateTimeNow = new Date();  // Gets the current time
  var timeStampNow = Math.floor(dateTimeNow.getTime() / 1000);
  var seconds = timeStampNow - timeStamp;

  if (seconds > 3 * 24 * 3600) {
    return `${Math.floor(seconds / 24 / 3600)} days ago`;
  }
  // more that two days
  if (seconds > 2 * 24 * 3600) {
    return `a few days ago`;
  }
  // a day
  if (seconds > 24 * 3600) {
    return `yesterday`;
  }
  if (seconds > 7200) {
    return `${Math.floor(seconds / 60 / 60)} hours ago`;
  }
  if (seconds > 3600) {
    return `a few hours ago`;
  }
  if (seconds > 1800) {
    return `Half an hour ago`;
  }
  if (seconds > 60) {
    return `${Math.floor(seconds / 60)} minutes ago`;
  }
}