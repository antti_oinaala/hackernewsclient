import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const Button = ({ story }) => {
  const onPress = () => {
    Linking.openURL(`${story.url}`)
  }

  return (
    <TouchableOpacity style={styles.button} onPress={onPress}>
      <View style={styles.buttonBackground}>
        <Icon name={'link'} size={14} color={'black'} />
        <Text style={styles.buttonTitle}>View on Website</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'transparent',
    borderRadius: 2,
    borderWidth: 1,
    borderColor: 'grey',
    padding: 3,
  },
  buttonBackground: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  title: {
    color: 'black',
    fontSize: 14,
  },
});

export default Button;