import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, SafeAreaView, FlatList, StyleSheet } from 'react-native'

import CommentListItem from './CommentListItem'
import { fetchTopStory } from '../../services/HackerNewsAPI'

const CommentScreen = ({ navigation }) => {
  const [isLoading, setIsLoading] = useState(false)
  const [comments, setComments] = useState([])

  const story = navigation.getParam('story', 'default value');

  useEffect(() => {
    setIsLoading(true);

    const promises = []

    const { kids } = story

    if (Array.isArray(kids)) {
      kids.map((id) => {
        promises.push(fetchTopStory(id))
      })

      Promise.all(promises)
        .then(comments => {
          setIsLoading(false);
          setComments(comments);
        })
    }

  }, []);

  const renderdItem = (comment) => {
    return <CommentListItem comment={comment} />;
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View>
          <Text style={styles.title}>{story.title}</Text>
          {
            isLoading ?
              <Text>Loading data...</Text> :
              <FlatList
                data={comments}
                keyExtractor={item => item.id.toString}
                renderItem={({ item }) => (
                  renderdItem(item)
                )}
              />
          }
        </View>
      </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 2,
  },
});

export default CommentScreen;
