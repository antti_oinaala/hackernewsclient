import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import Button from '../common/Button'
import { timeAgo } from '../../misc/Misc'

const ListItem = (props) => {
  const { story, onNavigation } = props;

  const time = timeAgo(story.time)

  const onPress = () => onNavigation(story)

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.mainContainer}>
        <View style={styles.imageContainer}>
          <Icon name={'hacker-news'} size={30} color={'lightgrey'} />
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.title}>{story.title}</Text>
          <Text style={styles.content}>{story.score} points by {story.by} | {time}</Text>
          <Button story={story} />
        </View>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 5,
    backgroundColor: '#ffffff'
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  infoContainer: {
    flex: 6,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#DDDDDD',
    borderRadius: 2,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 14,
    padding: 2,
  },
  content: {
    fontSize: 12,
    padding: 1,
  },
});

export default ListItem;