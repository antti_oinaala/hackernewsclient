import React, { useEffect, useState } from 'react'
import { StatusBar, SafeAreaView, View, FlatList, Text, TouchableOpacity } from 'react-native'

import { fetchTopStoriesIDs, fetchTopStory } from '../../services/HackerNewsAPI'
import ListItem from './ListItem'


const TopStoriesScreen = ({ navigation }) => {
  const [isLoading, setIsLoading] = useState(false)
  const [topStories, setTopStories] = useState([])

  useEffect(() => {
    setIsLoading(true);

    fetchTopStoriesIDs()
      .then((ids) => {
        console.log('ids length ' + ids.length);

        const promises = []

        ids.slice(0, 30).map((id) => {
          promises.push(fetchTopStory(id))
        })

        Promise.all(promises)
          .then(stories => {
            stories.sort((x, y) => y.time - x.time);
            setIsLoading(false);
            setTopStories(stories);
          })
      })
  }, []);

  const onNavigation = (story) => navigation.navigate('Comments', { story: story });

  const renderdItem = (story) => {
    console.log('Story Title ' + story.title);
    return (
      <ListItem story={story} onNavigation={onNavigation} />
    )
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View>
          {
            isLoading ?
              <Text>Loading data...</Text> :
              <FlatList
                data={topStories}
                keyExtractor={item => item.id.toString}
                renderItem={({ item }) => (
                  renderdItem(item)
                )}
              />
          }

        </View>
      </SafeAreaView>
    </>
  )
}

export default TopStoriesScreen