import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import HTMLView from 'react-native-htmlview'

import { timeAgo } from '../../misc/Misc'

const CommentListItem = ({ comment }) => {
  if (comment !== undefined) {
    const time = timeAgo(comment.time)

    return (
      <View style={styles.container}>
        <View style={styles.commentContainer}>
          <HTMLView value={`${comment.text}`} stylesheet={styles.content} />
        </View>
        <Text style={styles.footer}>{time}</Text>
      </View>
    )
  }

  return <View><Text>No comment</Text></View>
}

const styles = StyleSheet.create({
  container: {
    padding: 3,
  },
  commentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 5,
    backgroundColor: '#ffffff'
  },
  content: {
    fontSize: 12,
    padding: 5,
  },
  footer: {
    fontWeight: 'bold',
    fontSize: 12,
    padding: 2,
    backgroundColor: '#fff',
  },
});

export default CommentListItem;