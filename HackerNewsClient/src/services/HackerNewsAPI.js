import axios from 'axios';

export const fetchTopStoriesIDs = () => Promise.resolve().then(() => {
  return (
    axios
      .get(`https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty`)
      .then(response => response.data)
      .then(storyIds => {
        console.log('StoryIds ' + storyIds.length);
        console.log('Array? ' + Array.isArray(storyIds));

        return storyIds
      })
      .catch((error) => {
        console.log(error);
      })
  )
})

export const fetchTopStory = (id) => {
  // console.log('fetch id ' + id);
  return new Promise((resolve, reject) => {
    axios
      .get(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`)
      .then(response => response.data)
      .then(story => {
        console.log('New story');
        resolve(story)
      })
      .catch((error) => {
        console.log(error);
        reject(error)
      })
  })
}

